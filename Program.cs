﻿using Gooey;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace SqlServerScripter {
	static class Program {
		public static Utilities Utils = new Utilities();

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			SssConfig config = new SssConfig();
			try {
				// TODO: update SSS to work with latest versions of SQL Server...

				// Config sanity checks
				string maxOutputFileSizeBytesStr = ConfigurationManager.AppSettings["MaxOutputFileSizeBytes"];
				uint maxOutputFileSizeBytes;
				if (maxOutputFileSizeBytesStr == null) {
					config.MaxOutputFileSizeBytes = null;
				}
				else if (!TryParseHelper<uint>.TryParse(maxOutputFileSizeBytesStr, out maxOutputFileSizeBytes)) {
					Program.Utils.ShowError("Couldn't parse app setting MaxOutputFileSizeBytes as a valid uint.");
					throw new Exception("Sanity check failed.");
				}
				else {
					config.MaxOutputFileSizeBytes = maxOutputFileSizeBytes;
				}
			}
			catch (Exception) {
				// App config sanity checking failed??  This is an unrecoverable error.
				return;
			}

			Application.Run(new frmMain(config));
		}
	}
}
